<?php

/*
Feed example
<SHOP>
  <SHOPITEM>
    <PRODUCTNAME>Some product name</PRODUCTNAME>
    <DESCRIPTION>Some product description</DESCRIPTION>
    <URL>https://somewhere.cz/product-url</URL>
    <IMGURL>https://somewhere.cz/product-image/?id=67803&type=THUMB</IMGURL>
    <PRICE>666</PRICE>
    <VAT>21</VAT>
    <PRICE_VAT>777</PRICE_VAT>
    <DELIVERY_DATE>0</DELIVERY_DATE>
    <MANUFACTURER>Manufacturer name</MANUFACTURER>
    <CATEGORYTEXT>CATEGORY | SUB CATEGORY</CATEGORYTEXT>
    <VARIANT>
      <PRODUCTNAMEEXT>AAA</PRODUCTNAMEEXT>
      <EAN>123456789</EAN>
    </VARIANT>
  </SHOPITEM>
</SHOP>
*/

ini_set('memory_limit', '256M');

$url = 'https://somewhere.cz/somefeed.xml';
$limit = 20;
$categoryDelimeter = '|';
$expectedCategoryDelimeter = '>';

function getItemCode($row) {
   $code = $radek->CODE;
   return $code;
}

// create curl resource
$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // return content as a string from curl_exec
curl_setopt($ch, CURLOPT_URL, $url); // set URL
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); // set HTTP method

$body = curl_exec($ch); // execute

curl_close($ch); // close curl resource to free up system resources

header('Content-type: text/xml; charset=utf-8');

libxml_use_internal_errors(true);
$data = simplexml_load_string($body);
if (!$data) {
    echo "Error loading XML";
    foreach(libxml_get_errors() as $error) {
      echo " ", $error->message;
    }
} else {
    echo "<SHOP>\n";
    $cnt = 0;
    foreach($data->children() as $radek) {
      if ($cnt > $limit) {
         break;
      }
      $cnt = $cnt + 1;

      $hasVariants = $radek->VARIANT->count() > 1;

      echo "<SHOPITEM>\n";
      echo "<NAME>" . str_replace("&", "&amp;", $radek->PRODUCTNAME) . "</NAME>\n";

      echo "<IMAGES>\n";
      $image = $radek->IMGURL;
      $image = str_replace("&", "&amp;", $image);
      //echo "<IMAGE>" . $image . "</IMAGE>\n";
      $image = str_replace("THUMB", "PREVIEW", $image);
      echo "<IMAGE>" . $image . "</IMAGE>\n";
      echo "</IMAGES>\n";

      echo "<CATEGORIES><CATEGORY>" . str_replace($categoryDelimeter, $expectedCategoryDelimeter, $radek->CATEGORYTEXT) . "</CATEGORY></CATEGORIES>\n";

      $description = str_replace("&", "&amp;", $radek->DESCRIPTION);
      $description = str_replace("\n", "<br/>", $description);
      echo "<DESCRIPTION><![CDATA[" . $description . "]]></DESCRIPTION>\n";

      if (empty($radek->MANUFACTURER) === FALSE) {
         echo "<MANUFACTURER>" . $radek->MANUFACTURER . "</MANUFACTURER>\n";
      }

      $code = getItemCode($radek);
      if ($hasVariants === FALSE) {
         echo "<CODE>" . $code . "</CODE>\n";
         echo "<EAN>" . $radek->VARIANT->EAN . "</EAN>\n";
         echo "<PRICE>" . $radek->PRICE . "</PRICE>\n";
         echo "<PRICE_VAT>" . $radek->PRICE_VAT . "</PRICE_VAT>\n";
         echo "<VAT>" . $radek->VAT . "</VAT>\n";
      } else {
         echo "<VARIANTS>\n";
         foreach($radek->VARIANT as $variant) {
            echo "<VARIANT>\n";
            echo "<CODE>" . $code . "/" . $variant->PRODUCTNAMEEXT . "</CODE>\n";
            echo "<EAN>" . $variant->EAN . "</EAN>\n";
            echo "<PRICE>" . $radek->PRICE . "</PRICE>\n";
            echo "<PRICE_VAT>" . $radek->PRICE_VAT . "</PRICE_VAT>\n";
            echo "<VAT>" . $radek->VAT . "</VAT>\n";

            echo "<PARAMETERS><PARAMETER><NAME>velikost</NAME><VALUE>" . $variant->PRODUCTNAMEEXT . "</VALUE></PARAMETER></PARAMETERS>\n";
            echo "</VARIANT>\n";
         }
         echo "</VARIANTS>\n";
      }

      echo "</SHOPITEM>\n";
    }
    echo "</SHOP>\n";
}
